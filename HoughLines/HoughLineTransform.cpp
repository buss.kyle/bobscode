#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <opencv2/opencv.hpp>
#include <stdio.h>

using namespace cv;
using namespace std;

int main(int argc, char** argv) {
  VideoCapture right(0); // dev(0) camera
  VideoCapture left(1); // dev(1) camera

  left.set(CAP_PROP_FPS, 1); // capture one frame
  right.set(CAP_PROP_FPS, 1); // capture one frame

   // Declare the output variables
   Mat right_dst, right_cdst, right_cdstP, left_dst, left_cdst, left_cdstP;

// checks to see if camera's are capturing data
   if(!right.isOpened() || !left.isOpened()) {
     printf("Webcam's did not open, check if sudo service motion has been stopped. Exiting now.");
     return -1;
   }

   // Loads captured image and converts to grayscale
   Mat right_src, left_src;

   right >> right_src;
   left >> left_src;

   // dev1 camera image needs flipped
   flip(left_src, left_src, -1);

   // Check if VideoCapture has been Mat-ed
   if (right_src.empty() || left_src.empty()) {
     printf("No image data received. Exiting now. \n");
     return -1;
   }

   // Canny Edge detection
   Canny(right_src, right_dst, 50, 200, 3);
   Canny(left_src, left_dst, 50, 200, 3);

   // Copy edges to the images that wil display the results in BGR
   cvtColor(right_dst, right_cdst, COLOR_GRAY2BGR);
   cvtColor(left_dst, left_cdst, COLOR_GRAY2BGR);
   // clone image for Standard and Probabilistic transforms
   right_cdstP = right_cdst.clone();
   left_cdstP = left_cdst.clone();

   // Standard Hough Line Transform
   vector<Vec2f> right_lines, left_lines; // will hold the results of the detection

   HoughLines(right_dst, right_lines, 1, CV_PI/180, 150, 0, 0); // runs the actual detection for right camera
   /*   the values after CV_PI/180, can be modified to adjust the threshold of line detection.
    *   150 is the threshold value mentioned in Info.txt. 0, 0 are default values -> see OpenCV docs for
    *   addtional information.
    */

   // Draw the lines
   for( size_t i = 0; i < right_lines.size(); i++) {
     float rho = right_lines[i][0], theta = right_lines[i][1];
     Point pt1, pt2;
     double a = cos(theta), b = sin(theta);
     double x0 = a * rho, y0 = b * rho;

     pt1.x = cvRound(x0 + 1000 * (-b));
     pt1.y = cvRound(y0 + 1000 * (a));
     pt2.x = cvRound(x0 - 1000 * (-b));
     pt2.y = cvRound(y0 - 1000 * (a));
     line( right_cdst, pt1, pt2, Scalar(0,0,255), 3, LINE_AA);
   }

   HoughLines(left_dst, left_lines, 1, CV_PI/180, 150, 0, 0); // runs the actual detection for left camera
   /*   the values after CV_PI/180, can be modified to adjust the threshold of line detection.
    *   150 is the threshold value mentioned in Info.txt. 0, 0 are default values -> see OpenCV docs for
    *   addtional information.
    */

   // Draw the lines
   for( size_t i = 0; i < left_lines.size(); i++) {
     float rho = left_lines[i][0], theta = left_lines[i][1];
     Point pt1, pt2;
     double a = cos(theta), b = sin(theta);
     double x0 = a * rho, y0 = b * rho;

     pt1.x = cvRound(x0 + 1000 * (-b));
     pt1.y = cvRound(y0 + 1000 * (a));
     pt2.x = cvRound(x0 - 1000 * (-b));
     pt2.y = cvRound(y0 - 1000 * (a));
     line( left_cdst, pt1, pt2, Scalar(0,0,255), 3, LINE_AA);
   }

   // Probabilistic Line Transform
   vector<Vec4i> right_linesP, left_linesP; // will hold the results of the detection
   HoughLinesP(right_dst, right_linesP, 1, CV_PI/180, 50, 50, 10); // runs the actual detection for right camera
   /*   the values after CV_PI/180, can be modified to adjust the threshold of line detection.
    *   Mentioned in Info.txt. 50 is the current threshold value, the next 50 is the current minLineLength value, and 10
    *   is the maxLineGap value. Adjusting these will modify line detection.
    */

   // Draw the lines
   for( size_t i = 0; i < right_linesP.size(); i++) {
     Vec4i l = right_linesP[i];
     line( right_cdstP, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0, 0, 255), 3, LINE_AA);
   }

   HoughLinesP(left_dst, left_linesP, 1, CV_PI/180, 50, 50, 10); // runs the actual detection for the left camera
   /*   the values after CV_PI/180, can be modified to adjust the threshold of line detection.
    *   Mentioned in Info.txt. 50 is the current threshold value, the next 50 is the current minLineLength value, and 10
    *   is the maxLineGap value. Adjusting these will modify line detection.
    */

   // Draw the lines
   for( size_t i = 0; i < left_linesP.size(); i++) {
     Vec4i l = left_linesP[i];
     line( left_cdstP, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0, 0, 255), 3, LINE_AA);
   }

   // save results of transformations for right camera
   imwrite("right_source.png", right_src);
   imwrite("right_stand_transform.png", right_cdst);
   imwrite("right_prob_transform.png", right_cdstP);

   // sve results of transformations for left camera
   imwrite("left_source.png", left_src);
   imwrite("left_stand_transform.png", left_cdst);
   imwrite("left_prob_transform.png", left_cdstP);

   return 0;
}
