#ifdef __cplusplus
extern "C" {
#endif

#ifndef DIST_FUNCTION_HPP__
#define DIST_FUNCTION_HPP__

class DistFunction {
  public:
  int calculateDist(int changeX);
};

#endif

#ifdef __cplusplus
}
#endif
