#include <math.h>
#include "DistFunction.hpp"

using namespace std;

const double K1 = 3998.12f;
const double K2 = -0.87f;

int DistFunction::calculateDist(int changeX) {
  // calcDist set to 0.0 means object is really close.
  int calcDist = 0;
  if (changeX >= 10 && changeX <= 100) {
  // physics calculation to determine objects distance from bot...
    calcDist = K1 * pow(changeX, K2);
  }
  return calcDist;
}
