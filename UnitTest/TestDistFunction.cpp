#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <math.h>
#include <string>
#include <list>
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/ui/text/TextTestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/CompilerOutputter.h>

#include "DistFunction.hpp"

using namespace CppUnit;
using namespace std;

class TestDistFunction : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE(TestDistFunction);
  CPPUNIT_TEST(calculateDistTest);
  CPPUNIT_TEST_SUITE_END();

public:
  void setUp(void);
  void tearDown(void);

protected:
  void calculateDistTest(void);

private:
  DistFunction *mTestObj;
};


void TestDistFunction::calculateDistTest(void) {
  CPPUNIT_ASSERT(0 == mTestObj->calculateDist(107));
  CPPUNIT_ASSERT(88 == mTestObj->calculateDist(80));
  CPPUNIT_ASSERT(107 == mTestObj->calculateDist(64));
  CPPUNIT_ASSERT(120 == mTestObj->calculateDist(56));
  CPPUNIT_ASSERT(140 == mTestObj->calculateDist(47));
  CPPUNIT_ASSERT(158 == mTestObj->calculateDist(41));
  CPPUNIT_ASSERT(181 == mTestObj->calculateDist(35));
  CPPUNIT_ASSERT(196 == mTestObj->calculateDist(32));
  CPPUNIT_ASSERT(213 == mTestObj->calculateDist(29));
  CPPUNIT_ASSERT(234 == mTestObj->calculateDist(26));
  CPPUNIT_ASSERT(261 == mTestObj->calculateDist(23));
  CPPUNIT_ASSERT(282 == mTestObj->calculateDist(21));
  CPPUNIT_ASSERT(295 == mTestObj->calculateDist(20));
  CPPUNIT_ASSERT(308 == mTestObj->calculateDist(19));
  CPPUNIT_ASSERT(339 == mTestObj->calculateDist(17));
  CPPUNIT_ASSERT(358 == mTestObj->calculateDist(16));
  CPPUNIT_ASSERT(379 == mTestObj->calculateDist(15));
  CPPUNIT_ASSERT(429 == mTestObj->calculateDist(13));
  CPPUNIT_ASSERT(460 == mTestObj->calculateDist(12));
  CPPUNIT_ASSERT(496 == mTestObj->calculateDist(11));
  CPPUNIT_ASSERT(539 == mTestObj->calculateDist(10));
  CPPUNIT_ASSERT(0 == mTestObj->calculateDist(9));
  CPPUNIT_ASSERT(0 == mTestObj->calculateDist(8));
}

void TestDistFunction::setUp(void) {
  mTestObj = new DistFunction();
}

void TestDistFunction::tearDown(void) {
  delete mTestObj;
}

CPPUNIT_TEST_SUITE_REGISTRATION(TestDistFunction);

int main(int argc, char* argv[]) {
  CPPUNIT_NS::TestResult testresult;

  CPPUNIT_NS::TestResultCollector collectedresults;
  testresult.addListener(&collectedresults);

  CPPUNIT_NS::BriefTestProgressListener progress;
  testresult.addListener(&progress);

  CPPUNIT_NS::TestRunner testrunner;
  testrunner.addTest(CPPUNIT_NS::TestFactoryRegistry::getRegistry().makeTest());
  testrunner.run(testresult);

  CPPUNIT_NS::CompilerOutputter compileroutputter(&collectedresults, std::cerr);
  compileroutputter.write();

  return collectedresults.wasSuccessful() ? 0 : 1;
}
