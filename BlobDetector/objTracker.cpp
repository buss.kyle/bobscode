#include <opencv2/core/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/features2d.hpp>
#include <vector>
#include <map>
#include <iostream>

using namespace cv;
using namespace std;


int main(int argc, char* argv[]) {
  VideoCapture left(0);   // dev0 camera - might be switched
  VideoCapture right(1);  // dev1 camera - might be switched

  left.set(CAP_PROP_FPS, 1); // capture one frame
  right.set(CAP_PROP_FPS, 1); // capture one frame

  if(!left.isOpened()) {  || !right.isOpened()) {
    cout << "Unable to open a webcam. Check if sudo service motion has been stopped? Exiting now." << endl;
    return -1;
  }

  Mat leftCam, hsvLeft, mask, leftVid, gray, rightCam, hsvRight, rightVid;

  float scalingFactor = 0.75;

  while(left.read(leftCam)) {
    leftVid = Scalar(0, 0, 0);
    rightVid = Scalar(0, 0, 0);

    left >> leftCam;
    right >> rightCam;

    // dev1 needs flipped
    flip(rightCam, rightCam, -1);

    if(leftCam.empty()) || rightCam.empty())
      break;

      resize(leftCam, leftCam, Size(), scalingFactor, scalingFactor, INTER_AREA);
      // resize(rightCam, rightCam, Size(), scalingFactor, scalingFactor, INTER_AREA);

      cvtColor(leftCam, gray, COLOR_BGR2GRAY); // converts to HSV color
      // cvtColor(rightCam, hsvRight, COLOR_BGR2HSV); // converts to HSV color

      // range for blue in HSV
      Scalar low = Scalar(60, 100, 100);
      Scalar high = Scalar(180, 255, 255);

      // inRange(hsvLeft, low, high, mask);
      // inRange(hsvRight, low, high, mask);
      bitwise_and(leftCam, leftCam, leftVid, mask=mask);
      bitwise_and(rightCam, rightCam, rightVid, mask=mask);
      medianBlur(leftVid, leftVid, 5);
      medianBlur(rightVid, rightVid,

      vector<Vec3f> circles;
      HoughCircles(gray, circles, HOUGH_GRADIENT, 1,
                   gray.rows/16,  // change this value to detect circles with different distances to each other
                   100, 30, 20, 60 // change the last two parameters
              // (min_radius & max_radius) to detect larger circles
      );

      for( size_t i = 0; i < circles.size(); i++ )
      {
          Vec3i c = circles[i];
          Point center = Point(c[0], c[1]);
          // circle center
          circle( leftCam, center, 1, Scalar(0,100,100), 3, LINE_AA);
          // circle outline
          int radius = c[2];
          circle( leftCam, center, radius, Scalar(255,0,255), 3, LINE_AA);
      }

       imWrite("left_cam.jpg", leftCam);
       imWrite("left_circle.jpg", leftVid);

       imWrite("right_cam.jpg", rightCam);
       imWrite("right_circle.jpg", rightVid);
  }
  return 0;
}
