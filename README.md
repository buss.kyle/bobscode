**piBot**

Semester Project repository for CSIT 404 and 441

This repository is for work completed by me on my piBot project in relation to CSIT 404 - Software Engineering and CSIT 441 - Introduction to Artificial Intelligence.

For the full piBot project respository of what has been completed by me and Dr. Trantham can be found at: https://github.com/tranthamkw/piBot

Updates of my project research, contributions, and code explainations can be found here: https://csit404-441.blogspot.com/